library(shiny)
# # # # # # # # # # # # # # # # # # # # # # # #
#                   U I                       #
# # # # # # # # # # # # # # # # # # # # # # # #
ui <- fluidPage(
  tags$head(tags$script(HTML('$(document).on("shiny:connected", function(){ document.getElementById("i").focus() });'))),
  titlePanel("Anagramize"),
  h4("A simple R app that anagramizes every word you type in."),

  fluidRow(
    column(12,
           "",
           textInput(
             inputId = "i",
             label = "Just type in anything and hit Enter to have R generate a random anagram",
             placeholder = "Go on and type in one or more words",
             width = "100%"
           ),
           textOutput(
             outputId = "o",
             inline = FALSE
           )
    )
  )
)
# # # # # # # # # # # # # # # # # # # # # # # #
#               S E R V E R                   #
# # # # # # # # # # # # # # # # # # # # # # # #
server <- function(input, output) {

  anagramize <- reactive({
    words <- unlist(strsplit(x = input$i, split = " "))

    res <- vector()

    for (word in words) {
      wordAsChars <- unlist(strsplit(x = word, split = ""))
      wordAnagramized <- sample(wordAsChars, length(wordAsChars))
      wordAnagramizedAsString <- paste0(wordAnagramized, collapse = "")
      res <- append(x = res, values = wordAnagramizedAsString)
    }
    return(res)
  })

  output$o = renderText({
    anagramize()
  })

}
# # # # # # # # # # # # # # # # # # # # # # # #
#                   R U N                     #
# # # # # # # # # # # # # # # # # # # # # # # #
shinyApp(ui = ui, server = server)
